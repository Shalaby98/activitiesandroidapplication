package ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import Model.Activities;
import Model.ActivitiesRepo;

public class ActivitiesViewModel extends AndroidViewModel {

    private ActivitiesRepo activitiesRepo;
    private LiveData<List<Activities>> activityList;
    private LiveData<Activities> activityID;
    private MutableLiveData<Boolean> state = new MutableLiveData<Boolean>();
    public LiveData<Boolean> booleanLiveData = state;


    public ActivitiesViewModel(@NonNull Application application) {
        super(application);
        activitiesRepo= new ActivitiesRepo(application);
        //activityList = activitiesRepo.getAllActivities();
    }

    public LiveData<List<Activities>> getAllActivities(){
        start();
        activityList = activitiesRepo.getAllActivities();
        end();
        return activityList;
    }

    public LiveData<List<Activities>> getCertainLocation(double longtitude, double latitude)
    {
        activityList = activitiesRepo.getCertainLocation(longtitude, latitude);
        return activityList;
    }

    public LiveData<List<Activities>> getNearByPlaces(double latitude,double longtitude)
    {
        activityList = activitiesRepo.getNearByPlaces(latitude,longtitude);
        return activityList;
    }

    public LiveData<Activities> getActivityByID(int id)
    {
        activityID = activitiesRepo.getActivityByID(id);
        return activityID;
    }

    public void start()
    {
        state.setValue(true);
    }
    public void end()
    {
        state.setValue(false);
    }
}
