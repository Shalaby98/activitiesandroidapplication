package com.example.activitiesproject;

public class Reservations {

    String name;
    String date;
    String time;
    String reservationNumber;

    public Reservations()
    {

    }

    public Reservations(String name, String date, String time,String reservationNumber)
    {
        this.name = name;
        this.date = date;
        this.time = time;
        this.reservationNumber = reservationNumber;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getReservationNumber() {
        return reservationNumber;
    }
}
