package com.example.activitiesproject.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.activitiesproject.Adapters.ActivitiesAdapter;
import com.example.activitiesproject.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import Model.Activities;
import ViewModel.ActivitiesViewModel;


public class MainFrag extends Fragment implements ActivitiesAdapter.itemOnClickListner, ActivityCompat.OnRequestPermissionsResultCallback {

    private ActivitiesViewModel viewModel;
    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    LifecycleOwner lifecycleOwner;
    List<Activities> viewActivitiesList;
    ActivitiesAdapter adapter;
    double latitude;
    double longtitude;
    boolean toastShown = false;
    boolean permissionGranted = false;
    Location locationGPS;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);


        RecyclerView recyclerView = view.findViewById(R.id.rvList);
        adapter = new ActivitiesAdapter(getContext());
        adapter.setItemListener(this);
        ActivitiesAdapter.ActivitiesViewHolder holder;
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        viewModel = ViewModelProviders.of(this).get(ActivitiesViewModel.class);


        viewModel.getAllActivities().observe(getActivity(), new Observer<List<Activities>>() {
            @Override
            public void onChanged(final List<Activities> activities) {
                viewActivitiesList = activities;
            }
        });

        requestPermissions(new String[]
                {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        return view;
    }

    @Override
    public void changeFragment(int id) {
        Navigation.findNavController(getView()).navigate(MainFragDirections.actionMainFragToDetailsFrag(id));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("GPSDisabled", "onRequestPermissionsResult: d5l frag");
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    locationGPS = getLastKnownLocation();
                    Log.d("locationGPS", locationGPS + "");
                    longtitude = locationGPS.getLongitude();
                    latitude = locationGPS.getLatitude();
                    Log.d("Latitude", String.valueOf(latitude));
                    Log.d("Longtitude", String.valueOf(longtitude));
                }
                permissionGranted = true;
                Log.d("GPSDisabled", "Permission Granted");
                //longtitude=30.221091357304115;
                //latitude=31.45469704400343;
                viewModel.getNearByPlaces(latitude, longtitude).observe(getActivity(), new Observer<List<Activities>>() {
                    @Override
                    public void onChanged(final List<Activities> activities) {
                        if (activities.size() == 0) {
                            if (!toastShown) {
                                Snackbar.make(getActivity().findViewById(android.R.id.content), "No Places found near you, Here are all the places", Snackbar.LENGTH_LONG).show();
                                toastShown = true;
                            }
                            Log.d("Returned List", "No places");
                            adapter.setActivitiesList(viewActivitiesList);
                        } else {
                            adapter.setActivitiesList(activities);
                        }
                    }
                });

            } else {
                Toast.makeText(getContext(), "Permission for location access, denied", Toast.LENGTH_LONG).show();
                viewModel.getAllActivities().observe(getActivity(), new Observer<List<Activities>>() {
                    @Override
                    public void onChanged(final List<Activities> activities) {
                        //viewActivitiesList=activities;
                        adapter.setActivitiesList(activities);
                    }
                });
                permissionGranted = false;
                Log.d("GPSDisabled", "Permission Denied");
            }
        }
    }

    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        //ocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


}


