package com.example.activitiesproject.Fragments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.example.activitiesproject.R;
import com.example.activitiesproject.Reservations;
import com.example.activitiesproject.databinding.FragmentReservationBinding;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ReservationFrag extends Fragment {

    FragmentReservationBinding binding;
    int month;
    int year;
    int day;
    int img;
    String hour;
    String workingHours;
    String name;
    String [] workingHoursSplit;
    View root;
    String  openHour;
    String  closeHour;
    boolean timeConditions = false;
    boolean dateConditions = false;
    boolean reservationConditions = true;
    List<Reservations> reservationsList;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    String CHANNEL_ID = "channel_1";
    int notification_id = 1;
    NotificationManagerCompat notificationManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentReservationBinding.inflate(getLayoutInflater());
        root = getActivity().findViewById(android.R.id.content);

        firebaseDatabase = FirebaseDatabase.getInstance("https://activitiesproject-d48d1-default-rtdb.firebaseio.com/");
        databaseReference = firebaseDatabase.getReference("reservation");

        //Toast.makeText(getContext(),"Please Choose Month First",Toast.LENGTH_LONG);
        Snackbar.make(root,"Please Choose Month First",Snackbar.LENGTH_LONG).show();

        ReservationFragArgs reservationFragArgs = ReservationFragArgs.fromBundle(getArguments());
        name = reservationFragArgs.getName();
        workingHours = reservationFragArgs.getWorkingHours();
        img = reservationFragArgs.getImg();

        workingHoursSplit = workingHours.split("\\s");
        openHour = workingHoursSplit[0];
        closeHour = workingHoursSplit[2];

        binding.name.setText(name);
        binding.workingHours.setText(workingHours);
        binding.imageView2.setImageResource(img);

        reservationsList = new ArrayList<>();


        ArrayAdapter<CharSequence> adapterMonth = ArrayAdapter.createFromResource(getContext(), R.array.month,android.R.layout.simple_spinner_item);
        adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> adapterDay30 = ArrayAdapter.createFromResource(getContext(), R.array.day30,android.R.layout.simple_spinner_item);
        adapterDay30.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> adapterDay31 = ArrayAdapter.createFromResource(getContext(), R.array.day31,android.R.layout.simple_spinner_item);
        adapterDay31.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> adapterFeb = ArrayAdapter.createFromResource(getContext(), R.array.dayFeb,android.R.layout.simple_spinner_item);
        adapterFeb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> adapterHour = ArrayAdapter.createFromResource(getContext(), R.array.hour,android.R.layout.simple_spinner_item);
        adapterHour.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        binding.hourID.setAdapter(adapterHour);
        binding.hourID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hour = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        binding.monthID.setAdapter(adapterMonth);
        binding.monthID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                month = Integer.parseInt(parent.getItemAtPosition(position).toString());
                year = Integer.parseInt(binding.currentYear.getText().toString());
                if (month == 4 || month == 6 || month == 9 || month == 11)
                    binding.dayID.setAdapter(adapterDay30);
                else if(month == 2 && (year % 4) != 0)
                    binding.dayID.setAdapter(adapterFeb);
                else
                    binding.dayID.setAdapter(adapterDay31);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.dayID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                day = Integer.parseInt(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        binding.reserveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDate();
                try {
                    checkTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                checkConditions();
            }
        });

        return binding.getRoot();
    }

    public void checkDate()
    {
        Calendar calendar = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance(DateFormat.DATE_FIELD).format(calendar.getTime());
        String[] currentDateSplit = currentDate.split("/");
        int currentDay = Integer.parseInt(currentDateSplit[0]);
        int currentMonth = Integer.parseInt(currentDateSplit[1]);

        if(month < currentMonth)
        {
            Log.d("ChosenMonth","Entered IF something Wrong");
            dateConditions = false;
        }
        else if(day-currentDay<0)
        {
            if(month == currentMonth)
            {
                dateConditions = false;
            }
            else
                dateConditions = true;
        }
        else
            dateConditions = true;
    }

    public void dataBase()
    {
        String date= String.valueOf(day)+"/"+String.valueOf(month)+"/"+"2021";
        String number= databaseReference.push().getKey();
        if(checkReservation())
        {
            Reservations reservations = new Reservations(name,date,hour,number);
            databaseReference.child(String.valueOf(number)).setValue(reservations);
            sendNotification(number);
            Snackbar.make(root,"Reservation Completed",Snackbar.LENGTH_SHORT).show();
        }
        else
        {
            final Snackbar snackbar = Snackbar.make(root,"Time slot is not empty",Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Close", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        }


    }

    public void checkTime() throws ParseException {
        String string1 = openHour;
        Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(time1);
        //calendar1.set(2021,5,3);
        calendar1.add(Calendar.DATE, 1);
        Log.d("calenders",time1+"");


        String string2 = closeHour;
        int y= Integer.parseInt(closeHour.split(":")[0]);
        Date time2 = new SimpleDateFormat("HH:mm").parse(string2);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(time2);
        calendar2.add(Calendar.DATE, 1);
        Log.d("calenders",time2+"");

        String someRandomTime = hour;
        Date d = new SimpleDateFormat("HH:mm").parse(someRandomTime);
        Calendar calendar3 = Calendar.getInstance();
        calendar3.setTime(d);
        calendar3.add(Calendar.DATE, 1);

        Date x = calendar3.getTime();
        if((y <= 11) && (y >= 1) )
        {
            if (x.after(calendar1.getTime()) || x.before(calendar2.getTime())) {
                timeConditions = true;
            }
            else
                timeConditions = false;
        }
        else
        {
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                timeConditions = true;
            }
            else
                timeConditions = false;
        }


    }

    public void checkConditions()
    {
        if(!timeConditions || !dateConditions)
        {
            if(!timeConditions)
            {
                final Snackbar snackbar = Snackbar.make(root,name+" is not opened at that time",Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
                snackbar.show();
            }
            else if(!dateConditions)
            {
                final Snackbar snackbar = Snackbar.make(root,"Can't do reservation on a day that past",Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
                snackbar.show();
            }
        }
        else
            dataBase();
    }


    public boolean checkReservation()
    {
        getData();
        boolean check = true;
        for(int i=0;i<reservationsList.size();i++)
        {
            String date= String.valueOf(day)+"/"+String.valueOf(month)+"/"+"2021";
            if(name.equals(reservationsList.get(i).getName()) && hour.equals(reservationsList.get(i).getTime()) && date.equals(reservationsList.get(i).getDate()))
            {
                check =false;
                return check;
            }
        }
        return check;
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                reservationsList.clear();
                for(DataSnapshot snapshot: dataSnapshot.getChildren())
                {
                    Reservations reservations1= snapshot.getValue(Reservations.class);
                    reservationsList.add(reservations1);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getData()
    {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                reservationsList.clear();
                for(DataSnapshot snapshot: dataSnapshot.getChildren())
                {
                    Reservations reservations1= snapshot.getValue(Reservations.class);
                    reservationsList.add(reservations1);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void sendNotification(String id)
    {
        NotificationCompat.Builder builder;
        notificationManager = NotificationManagerCompat.from(getContext());
        createNotificationChannel();
        builder = new NotificationCompat.Builder(getContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_check_circle_24)
                .setContentTitle("Reservation Completed")
                .setContentText("Here is your reservation number: "+id)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        notificationManager.notify(notification_id, builder.build());
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);

        } else {
            Log.d("notification", "==== " + "NotificationChannel was not created");
        }
    }


}

