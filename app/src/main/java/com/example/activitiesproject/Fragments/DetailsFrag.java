package com.example.activitiesproject.Fragments;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.activitiesproject.Adapters.ImagesAdapter;
import com.example.activitiesproject.Adapters.TextViewAdapter;
import com.example.activitiesproject.databinding.FragmentDetailsBinding;

import Model.Activities;
import ViewModel.ActivitiesViewModel;

public class DetailsFrag extends Fragment  {

    FragmentDetailsBinding binding;

    private static final int REQUEST_CODE = 101;

    private ActivitiesViewModel viewModel;
    int id;
    float latitude;
    float longitude;
    int img;
    String name;
    String workingHours;
    ImagesAdapter adapter;
    TextViewAdapter tvAdapter;
    Activities activityImageList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*name.setText(activitiesItem.getName());
        phoneNumber.setText(activitiesItem.getPhoneNumber());
        workingStatus.setText(activitiesItem.getWorkingHours());
        img.setImageResource(activitiesItem.getImg());*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailsBinding.inflate(getLayoutInflater());
        binding.description.setMovementMethod(new ScrollingMovementMethod());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        adapter = new ImagesAdapter(getContext());
        binding.rvImages.setAdapter(adapter);
        binding.rvImages.setLayoutManager(layoutManager);

        tvAdapter = new TextViewAdapter(getContext());
        //binding.descRV.setAdapter(tvAdapter);
        //binding.descRV.setLayoutManager(new LinearLayoutManager(getContext()));

        DetailsFragArgs detailsFragArgs = DetailsFragArgs.fromBundle(requireArguments());
        id = detailsFragArgs.getId();
        Log.d("IDValue", String.valueOf(id));

        viewModel = ViewModelProviders.of(this).get(ActivitiesViewModel.class);
        viewModel.getActivityByID(id).observe(getActivity(), new Observer<Activities>() {
            @Override
            public void onChanged(Activities activities) {
                if(activities != null)
                {
                    binding.nameDetails.setText(activities.getName());
                    binding.phoneNumberDetails.setText(activities.getPhoneNumber());
                    binding.description.setText(getString(activities.getDescription()));
                    //tvAdapter.setDesc(getString(activities.getDescription()));
                    latitude = (float) activities.getLatitude();
                    longitude = (float) activities.getLongtitude();
                    name = activities.getName();
                    img = activities.getImg();
                    workingHours = activities.getWorkingHours();
                    int list [] = {activities.getImg(), activities.getImageID1(),activities.getImageID2(),activities.getImageID3(),activities.getImageID4()};
                    //activityImageList = activities;
                    adapter.setImagesList(list);
                }

            }
        });

        //int list [] = {activityImageList.getImg(), activityImageList.getImageID1(), activityImageList.getImageID2(), activityImageList.getImageID3(), activityImageList.getImageID4()};


        binding.locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getView()).navigate(DetailsFragDirections.actionDetailsFragToMapFragment(longitude,latitude,name));
            }
        });

        binding.reserveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Navigation.findNavController(getView()).navigate(DetailsFragDirections.actionDetailsFragToReservationFrag(name,workingHours,img));
            }
        });



        return binding.getRoot();
    }

}