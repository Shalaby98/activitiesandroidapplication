package com.example.activitiesproject.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.activitiesproject.R;

public class TextViewAdapter extends RecyclerView.Adapter<TextViewAdapter.TextViewHolder> {

    private final LayoutInflater mInflater;
    private String desc;

   public TextViewAdapter(Context context){mInflater = LayoutInflater.from(context);}

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = mInflater.inflate(R.layout.rv_text,parent,false);
        return new TextViewHolder(itemView);
    }

    public void setDesc(String s)
    {
        Log.d("DescriptionActivity",s);
        desc=s;
        Log.d("DescriptionActivity",desc);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        holder.description.setText(desc);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class TextViewHolder extends RecyclerView.ViewHolder{

        private final TextView description;

        public TextViewHolder(final View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.descriptionTV);
        }
    }
}
