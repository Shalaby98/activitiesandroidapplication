package com.example.activitiesproject.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.activitiesproject.R;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImagesViewHolder> {

    private final LayoutInflater mInflater;
    private  int[] imagesList;

    public ImagesAdapter(Context context){mInflater = LayoutInflater.from(context);}

    @NonNull
    @Override
    public ImagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = mInflater.inflate(R.layout.rv_images,parent,false);
        return new ImagesViewHolder(itemView);
    }

    public void setImagesList(int[] list)
    {
        imagesList = list;
        Log.d("EnteredIF",imagesList.length + " ");
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesViewHolder holder, int position) {
        if (imagesList != null){
            Log.d("EnteredIFPosition",String.valueOf(position));
            holder.imageView.setImageResource(imagesList[position]);
        }

    }


    @Override
    public int getItemCount() {
        if(imagesList != null)
            return imagesList.length;
        else
            return 0;
    }

    class ImagesViewHolder extends RecyclerView.ViewHolder{

        private final ImageView imageView;

        public ImagesViewHolder(final View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.placeImage);
        }
    }
}
