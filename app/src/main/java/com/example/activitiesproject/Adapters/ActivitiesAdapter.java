package com.example.activitiesproject.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.activitiesproject.R;

import java.util.List;

import Model.Activities;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.ActivitiesViewHolder> {

    private final LayoutInflater mInflater;
    private List<Activities> activitiesList;
    private ActivitiesViewHolder holder;
    private int itemID;
    private itemOnClickListner itemListener;

    public ActivitiesAdapter(Context context) { mInflater = LayoutInflater.from(context);}

    @NonNull
    @Override
    public ActivitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = mInflater.inflate(R.layout.rv_item,parent,false);
        return new ActivitiesViewHolder(itemView);
    }

    public void setItemListener(itemOnClickListner itemListener) {
        this.itemListener = itemListener;
    }

    @Override
    public void onBindViewHolder(ActivitiesViewHolder holder,int position)
    {
        if(activitiesList != null)
        {
            final Activities current = activitiesList.get(position);
            holder.name.setText(current.getName());
            holder.phoneNumber.setText(current.getPhoneNumber());
            holder.workingStatus.setText(String.valueOf(current.getWorkingHours()));
            holder.img.setImageResource(current.getImg());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.changeFragment(current.getID());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(activitiesList != null)
            return activitiesList.size();
        else
            return 0;
    }

    public void setActivitiesList(List<Activities> activitiesList1)
    {
        activitiesList = activitiesList1;
        notifyDataSetChanged();
    }

    public interface itemOnClickListner{
        public void changeFragment(int id);
    }

    public class ActivitiesViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView name;
        private final TextView phoneNumber;
        private final TextView workingStatus;
        private final ImageView img;

        public ActivitiesViewHolder(final View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.nameDetails);
            phoneNumber = itemView.findViewById(R.id.phoneNumberDetails);
            workingStatus = itemView.findViewById(R.id.workingStatusDetails);
            img = itemView.findViewById(R.id.img);
        }

    }
}
