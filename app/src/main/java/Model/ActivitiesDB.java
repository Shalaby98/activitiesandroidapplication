package Model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.activitiesproject.R;

@Database(entities = {Activities.class},version =1,exportSchema = false)
public abstract class ActivitiesDB extends RoomDatabase {
    public abstract ActivitiesDAO activitiesDAO();

    public static ActivitiesDB INSTANCE;
    static Resources r = Resources.getSystem();


    public static ActivitiesDB getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ActivitiesDB.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ActivitiesDB.class, "activityDB")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    @SuppressLint("StaticFieldLeak")
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final ActivitiesDAO aDao;


        Activities activities =
                new Activities("Autovroom",30.221091357304115, 31.45469704400343, "01200004485","15:00 - 24:00",R.drawable.av,R.drawable.av2,R.drawable.av1,R.drawable.av3,R.drawable.av4,R.string.autovroom);
        Activities activities2 =
                new Activities("Gravity Code",30.085229524164443, 31.364820289191155,"01098914275","10:00 - 23:55",R.drawable.gc,R.drawable.gc2,R.drawable.gc4,R.drawable.gc3,R.drawable.gc1,R.string.gravitycode);
        Activities activities3 =
                new Activities("Immersive VR",30.035472307744666, 31.506475043835135,"01020100044","14:00 - 2:00",R.drawable.vr,R.drawable.vr4,R.drawable.vr1,R.drawable.vr2,R.drawable.vr3,R.string.immersiveVR);
        Activities activities4 =
                new Activities("FootPark",30.039399381925055, 31.49733671557634,"01206616015","13:00 - 21:00",R.drawable.fp,R.drawable.fp2,R.drawable.fp1,R.drawable.fp3,R.drawable.fp4,R.string.footpark);
        Activities activities5 =
                new Activities("C.O.D.E Paintball", 30.06627814465654, 31.489327755189482 ,"01092988861","12:00 - 22:00",R.drawable.code,R.drawable.code4,R.drawable.code3,R.drawable.code2,R.drawable.code1,R.string.code);
        Activities activities6 =
                new Activities("Trapped",30.029970377502984, 31.462107583269802,"01200004434","15:00 - 3:00",R.drawable.tr,R.drawable.tr1,R.drawable.tr2,R.drawable.tr3,R.drawable.tr4,R.string.trapped);
        Activities activities7 =
                new Activities("Ski Egypt",29.980599950578352, 31.020175883996558,"01000257509","10:00 - 22:00",R.drawable.se,R.drawable.se1,R.drawable.se2,R.drawable.se3,R.drawable.se4,R.string.skiEgypt);
        Activities activities8 =
                new Activities("Nile Kayak Club",29.978227282611908, 31.234465396537455,"01010013335","8:00 - 15:00",R.drawable.nk,R.drawable.nk1,R.drawable.nk2,R.drawable.nk3,R.drawable.nk4,R.string.nilekayak);
        Activities activities9 =
                new Activities("IBC",30.07405076236323, 31.308307081551504,"0222612121","10:00 - 24:00",R.drawable.ibc,R.drawable.ibc1,R.drawable.ibc2,R.drawable.ibc3,R.drawable.ibc4,R.string.ibc);
        Activities activities10 =
                new Activities("Matarma Bay",29.49464603195035, 32.77776371186489,"01025075888","1:00 - 24:00",R.drawable.mb1,R.drawable.mb5,R.drawable.mb2,R.drawable.mb3,R.drawable.mb4,R.string.matarma);


        Activities[] activityList= {activities,activities2,activities3,activities4,activities5,activities6,activities7,activities8,activities9,activities10};

        PopulateDbAsync(ActivitiesDB db) {
            aDao = db.activitiesDAO();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            // Start the app with a clean database every time.
            // Not needed if you only populate the database
            // when it is first created
            aDao.deleteAll();

            for (int i = 0; i <= activityList.length - 1; i++) {
                aDao.insert(activityList[i]);
            }
            return null;
        }
    }
}
