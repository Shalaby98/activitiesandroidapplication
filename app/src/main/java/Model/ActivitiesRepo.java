package Model;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ActivitiesRepo {

    private ActivitiesDAO activitiesDAO;
    private LiveData<List<Activities>> activityList;
    private LiveData<Activities> activityID;

    public ActivitiesRepo(Application application)
    {
        ActivitiesDB db =ActivitiesDB.getDatabase(application);
        activitiesDAO= db.activitiesDAO();
        //activityList = activitiesDAO.getAllActivities();
    }

    public LiveData<List<Activities>> getAllActivities()
    {
        activityList = activitiesDAO.getAllActivities();
        return activityList;
    }

    public LiveData<List<Activities>> getCertainLocation(double latitude,double longtitude)
    {
        activityList = activitiesDAO.getCertainLocation(latitude,longtitude);
        return activityList;
    }

    public LiveData<List<Activities>> getNearByPlaces(double latitude,double longtitude)
    {
        activityList = activitiesDAO.getNearbyPlaces(latitude,longtitude);
        return activityList;
    }

    public LiveData<Activities> getActivityByID(int id)
    {
        activityID = activitiesDAO.getActivityByID(id);
        return activityID;
    }

}
