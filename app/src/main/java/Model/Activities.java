package Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "activities")
public class Activities {

    @ColumnInfo(name= "name")
    private String name;

    @ColumnInfo(name= "img")
    private int img;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name= "ID")
    private int ID;

    @ColumnInfo(name= "longitude")
    private double longtitude;

    @ColumnInfo(name= "latitude")
    private double latitude;

    @ColumnInfo(name= "phoneNo")
    private String phoneNumber;

   @ColumnInfo(name= "workingHours")
   private String workingHours;

   private int imageID1;

   private int imageID2;

   private int imageID3;

   private int imageID4;

   private int description;

    public Activities(String name,double latitude, double longtitude,String phoneNumber,String workingHours,int img,int imageID1,int imageID2,int imageID3,int imageID4,int description)
    {
        this.name=name;
        this.latitude= latitude;
        this.longtitude= longtitude;
        this.phoneNumber = phoneNumber;
        this.workingHours = workingHours;
        this.img=img;
        this.imageID1 = imageID1;
        this.imageID2 = imageID2;
        this.imageID3 = imageID3;
        this.imageID4 = imageID4;
        this.description = description;
    }

    public String getName(){ return this.name;}

    public int[] getImageIDs() {
        int [] imageIDs = {img,imageID1,imageID2};
        return imageIDs;
    }

    public String getPhoneNumber(){ return this.phoneNumber;}

    public double getLongtitude(){ return this.longtitude;}

    public double getLatitude(){return this.latitude;}

    public String getWorkingHours(){ return this.workingHours;}

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongtitude(double longitude) {
        this.longtitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getImg(){ return this.img;}

    public void setImg(int img) {
        this.img = img;
    }

    public int getImageID1() {
        return imageID1;
    }

    public int getImageID2() {
        return imageID2;
    }

    public int getImageID3() {
        return imageID3;
    }

    public int getImageID4() {
        return imageID4;
    }

    public void setImageID1(int imageID1) {
        this.imageID1 = imageID1;
    }

    public void setImageID2(int imageID2) {
        this.imageID2 = imageID2;
    }

    public void setImageID3(int imageID3) {
        this.imageID3 = imageID3;
    }

    public void setImageID4(int imageID4) {
        this.imageID4 = imageID4;
    }

    public int getDescription() {
        return description;
    }
}
