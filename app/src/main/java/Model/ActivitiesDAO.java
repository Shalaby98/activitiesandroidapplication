package Model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ActivitiesDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Activities activity);

    @Query("DELETE FROM activities")
    void deleteAll();

    @Query("SELECT * FROM activities")
    LiveData<List<Activities>> getAllActivities();

    @Query("SELECT * FROM activities WHERE latitude = :latitude AND longitude = :longtitude")
    LiveData<List<Activities>> getCertainLocation(double latitude, double longtitude);

    @Query("SELECT * FROM activities WHERE abs(latitude - :latitude) < 0.1 AND abs(longitude - :longtitude) < 0.3")
    LiveData<List<Activities>> getNearbyPlaces(double latitude, double longtitude);

    @Query("SELECT * FROM activities WHERE ID = :id")
    LiveData<Activities> getActivityByID(int id);
}
